#pragma once
#include "include/gameapi.h"
#include "namedef.h"
#include <time.h>




/*
 globalni Asset Manager objekt. 
 ucitava sve texture s diska koristeci I_cBitmap::Load funkciju i prenoseci
 GLuint za tu texturu kao prametar.
*/




class I_cAssetManager
{
 public:
        I_cAssetManager();
       ~I_cAssetManager();
       
        int LoadAssets(void);
       
       
        int LoadPlayer(void);
        int LoadFloor(void);
        int LoadWall(void);
        int LoadGUI(void);
        int LoadObjects(void);
        int LoadPanel(void);
        int LoadInventory(void);
        int LoadPanelImages(void);
        
        int LoadTextPNG(void);
        GLuint GetTextureAsset(GAMEAPI_TILE_ID asset);
        
        
        
 //private:        
         //GLuint assetList[GAMEAPI_MAX_ASSETS];
         GLuint playerAsset[16];
         GLuint floorAsset[6];
         GLuint wallAsset[4];
         GLuint guiAsset[5];
         GLuint objectAsset[2];
         GLuint testAsset[8];
         GLuint panelAsset[1];
         GLuint invAsset[1];
         GLuint npcAsset[1];
         
         GLuint tmp_guiAsset;
         
         GLuint GetRandomFloorTex(void);
         GLuint GetRandomWallTex(void);
         GLuint GetRandomObjectTex(void);
         
 private:
         int LoadAssetAsGLTexture(string path, GLuint *texture_id);     
 private:
          SDL_Surface *tmp_sdl_surface;       
          SDL_Rect tmp_sdl_rect; // kod ucitavanja png-a
         
};



extern I_cAssetManager *gAssetManager;