#include "I_cAssetManager.h"
#include "I_cGlobal.h"


I_cAssetManager *gAssetManager = 0;


I_cAssetManager::I_cAssetManager()
{

}


I_cAssetManager::~I_cAssetManager()
{
}




//======================================================================================// 
//===================[ LOAD IMAGE FROM DISK ]===========================================// 
//======================================================================================// 
// Ucitava bmp sliku s diska. koristeci SDL_Image lib
// openGL koristi texture tako da ih binda na GLuint texID
// Prvo se generira textura s glGenTextures(1,texture);
// i onda se binda na texture_id koji je prametar u ovoj funkciji. 
// Asset Manager dalje sprema tex_id u svoje liste text_Id-eva
//======================================================================================// 
int I_cAssetManager::LoadAssetAsGLTexture(string path, GLuint *texture_id)
{

 GLenum texture_format =0;
 GLint  nOfColors=0;
 
 tmp_sdl_surface = IMG_Load(path.c_str());
 if (tmp_sdl_surface == NULL)
    {
     gData->AppendLog("I_cAssetManager::LoadAssetAsGLTexture: ERROR: tmp_sdl_surface == NULL!");
     return -1;
    }

 nOfColors = tmp_sdl_surface->format->BytesPerPixel;
 if (nOfColors == 4)     // contains an alpha channel
    {
     if (tmp_sdl_surface->format->Rmask == 0x000000ff)
         texture_format = GL_RGBA;
     else 
         texture_format = GL_BGRA;
   } else if (nOfColors == 3)     // no alpha channel
             {
              if (tmp_sdl_surface->format->Rmask == 0x000000ff)
                  texture_format = GL_RGB;
              else
                  texture_format = GL_BGR;
            } else {
                    gData->AppendLog("I_cAssetManager::LoadAssetAsGLTexture: ERROR: the image is not truecolor..  this will probably break\n");
                    SDL_FreeSurface(tmp_sdl_surface);
                    return -1;
                   } 

 GLuint tmp = 0;
 // generiraj tex_id za ovaj surface
 glGenTextures(1,&tmp);
 *texture_id = tmp;
 glBindTexture(GL_TEXTURE_2D, *texture_id);

 
 // Set the texture's stretching properties
 glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
 glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR ); 
 // postavi postavke texture za openGL
 glTexImage2D( GL_TEXTURE_2D, 0, nOfColors, tmp_sdl_surface->w, tmp_sdl_surface->h, 0,texture_format, GL_UNSIGNED_BYTE, tmp_sdl_surface->pixels ); 

 // velicina slike
 tmp_sdl_rect.x = 0;
 tmp_sdl_rect.y = 0;
 tmp_sdl_rect.w = tmp_sdl_surface->w;
 tmp_sdl_rect.h = tmp_sdl_surface->h;

 // vise na netreba sdl surface u memoriji.
 if (tmp_sdl_surface) 
	 SDL_FreeSurface(tmp_sdl_surface);
    
 return 0;
}
//______________________________________________________________________________________\\ 

int I_cAssetManager::LoadAssets(void)
{
 int err = 0;
 err = LoadPlayer();
 if (err < 0)
    {
     gData->AppendLog("I_cAssetManager::LoadAssets: ERROR: LoadPlayer() failed!");
     return -1;
    } 
  
 err = LoadFloor();
 if (err < 0)
    {
     gData->AppendLog("I_cAssetManager::LoadAssets: ERROR: LoadFloor() failed!");
     return -1;
    } 
 
  
 err = LoadWall();
 if (err < 0)
    {
     gData->AppendLog("I_cAssetManager::LoadAssets: ERROR: LoadWall() failed!");
     return -1;
    }  
   
   
 err = LoadGUI();
 if (err < 0)
    {
     gData->AppendLog("I_cAssetManager::LoadAssets: ERROR: LoadGUI() failed!");
     return -1;
    }  
 
    
 err = LoadObjects();
 if (err < 0)
    {
     gData->AppendLog("I_cAssetManager::LoadAssets: ERROR: LoadObjects() failed!");
     return -1;
    }  
 
     
 err = LoadTextPNG();
 if (err < 0)
    {
     return -1;
     return (0);
    }  
    
 
      
 err = LoadPanelImages();
 if (err < 0)
    {
     return -1;
     return (0);
    }  
    
 
 
 
     
 err = LoadPanel();
 if (err < 0)
    {
     gData->AppendLog("MAIN: LoadAssets failed!");
     return -1;
    }  

 err = LoadInventory();
 if (err < 0)
    {
     gData->AppendLog("MAIN: LoadAssets failed!");
     return -1;
    }  
        
    
    

 return 0;
}


int I_cAssetManager::LoadPlayer()
{
 
 string resdir = "data/tiles/entity/player/";
 
 

 for (int c=0;c<16;c++)
     {
      string pth = resdir + gData->toString(c) + ".png";
      int err = 0;
      err = LoadAssetAsGLTexture(pth, &playerAsset[c]);
      if (err < 0)
         {
          //gData->AppendLog("ASSET LOAD: Error on loading asset %s. Defaulting to EMPTY.",gameapiTextureName[c].name);
          return -1;
         }
     }
 return 0;
}



int I_cAssetManager::LoadFloor()
{
 
 string resdir = "data/tiles/floor/";
 
 

 for (int c=0;c<6;c++)
     {
      string pth = resdir + gData->toString(c) + ".bmp";
      int err = 0;
      err = LoadAssetAsGLTexture(pth, &floorAsset[c]);
      if (err < 0)
         {
          //gData->AppendLog("ASSET LOAD: Error on loading asset %s. Defaulting to EMPTY.",gameapiTextureName[c].name);
          return -1;
         }
     }
 return 0;
}




int I_cAssetManager::LoadPanelImages()
{
 
 string resdir = "data/npc/";
 
 
      string pth = resdir + "guts.png";
      int err = 0;
      err = LoadAssetAsGLTexture(pth, &npcAsset[0]);
      if (err < 0)
         {
          //gData->AppendLog("ASSET LOAD: Error on loading asset %s. Defaulting to EMPTY.",gameapiTextureName[c].name);
          return -1;
         }
     
 return 0;
}


int I_cAssetManager::LoadPanel(void)
{
 string resdir = "data/tiles/panel/";
 
 
      string pth = resdir + "panel.png";
      int err = 0;
      err = LoadAssetAsGLTexture(pth, &panelAsset[0]);
      if (err < 0)
         {
          //gData->AppendLog("ASSET LOAD: Error on loading asset %s. Defaulting to EMPTY.",gameapiTextureName[c].name);
          return -1;
         }
     
 return 0;
}




int I_cAssetManager::LoadInventory()
{
 
 string resdir = "data/tiles/else/";
 
 
      string pth = resdir + "wall.png";
      int err = 0;
      err = LoadAssetAsGLTexture(pth, &invAsset[0]);
      if (err < 0)
         {
          //gData->AppendLog("ASSET LOAD: Error on loading asset %s. Defaulting to EMPTY.",gameapiTextureName[c].name);
          return -1;
         }
     
 return 0;
}



int I_cAssetManager::LoadWall()
{
 
 string resdir = "data/tiles/wall/";
 
 

 for (int c=0;c<4;c++)
     {
      string pth = resdir + gData->toString(c) + ".bmp";
      int err = 0;
      err = LoadAssetAsGLTexture(pth, &wallAsset[c]);
      if (err < 0)
         {
          //gData->AppendLog("ASSET LOAD: Error on loading asset %s. Defaulting to EMPTY.",gameapiTextureName[c].name);
          return -1;
         }
     }
 return 0;
}


int I_cAssetManager::LoadGUI(void)
{
 string resdir = "data/tiles/gui/";
 
 

 for (int c=0;c<5;c++)
     {
      string pth = resdir + gData->toString(c) + ".bmp";
      int err = 0;
      err = LoadAssetAsGLTexture(pth, &guiAsset[c]);
      if (err < 0) return -1;
     } 
 
 //string  pth = "data/tiles/gui/0.bmp";
 //
 //int err = LoadAssetAsGLTexture(pth, &tmp_guiAsset);
 //if (err < 0) return -1; 
 
 
 for (int c=0;c<5;c++)
     {
      printf("gui[%d] = %d\n",c,guiAsset[c]);
     }
 return 0;
}


int I_cAssetManager::LoadObjects(void)
{
 string resdir = "data/tiles/objects/";
 
 

 for (int c=0;c<2;c++)
     {
      string pth = resdir + gData->toString(c) + ".png";
      int err = 0;
      err = LoadAssetAsGLTexture(pth, &objectAsset[c]);
      if (err < 0)
         {
          //gData->AppendLog("ASSET LOAD: Error on loading asset %s. Defaulting to EMPTY.",gameapiTextureName[c].name);
          return -1;
         }
     } 
}


int I_cAssetManager::LoadTextPNG(void)
{
 string resdir = "data/tiles/test/";
 
 

 for (int c=0;c<8;c++)
     {
      string pth = resdir + gData->toString(c) + ".png";
      int err = 0;
      err = LoadAssetAsGLTexture(pth, &testAsset[c]);
      if (err < 0)
         {
          //gData->AppendLog("ASSET LOAD: Error on loading asset %s. Defaulting to EMPTY.",gameapiTextureName[c].name);
          return -1;
         }
     } 
}


GLuint I_cAssetManager::GetTextureAsset(GAMEAPI_TILE_ID asset)
{
 //GLuint asst =  assetList[asset];
 //return asst;
 return 0;
}


GLuint I_cAssetManager::GetRandomFloorTex(void)
{

 
 return floorAsset[rand() % 6];
}


GLuint I_cAssetManager::GetRandomWallTex(void)
{

 
 return wallAsset[rand() % 4];
}


GLuint I_cAssetManager::GetRandomObjectTex(void)
{
 return objectAsset[rand() % 2];
}



