#pragma once
#include "gameapi.h"
#include "I_cTile.h"

/*
 SPRITE
 nasljedjuje I_cTile klasu koja ima virtual void Draw().
 
 Draw ce postaviti I_cTile::SetTexture za svaki sljedeci tile i pozvati I_cTile::Draw();
 
 NOTE: 
      - Umjesto da nesljedjuje da li bi bilo bolje da ima vector<I_cTile*> ??
      
*/


class I_cSprite : public I_cTile
{
 public:
		I_cSprite();
	   ~I_cSprite();
	   
	    void Init(int posx, int posy);
       
       
        void SetAnimation(int anim_start, int anim_end);
        void StopAnimation();
        void Draw(int posx, int posy);

        int half_w,half_h;

        //vector<GLuint>textureList; 
        
        uint32_t timeDelay;
	    int currentTexture;
	    int currentAnim;
	    int currentAnimEnd;
	    bool stop;
	   
};