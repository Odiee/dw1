

2.7.2013
- Dodao GameAPI_Color u gameapi.txt
- I_cTextPanel sada koristi SetPanelColor za bojanje panela
- Dodao SetFont() u I_cTextPanel

5.2.2014
- izbrisao tilemaster.h, od sada se sve definicije trebaju nalaziti u gameapi.h
- I_cAssetManager vise ne nasljedjuje od I_cBitmap. 
- LoadAssetAsGLTexture se sada nalazi u I_cAssetManager umjesto I_cBitamp::Load()
- Sva error stanja su sada pregledana u I_cAssetManager::LoadAssetAsGLTexture
- I_cBitmap klasa vise nije dio projekta. Slobodno izbrisati
- Promijenio includove da imaju vise smisla u projektu. npr #include <SDL_Image/include/SDL_Image.h> unutar gameapi.h
- gAssetManager se sada stvara unutar I_cEngine::Init koji se zove iz I_cEngine::Start. 
  * Ne zvati openGL funkcije prije InitGL() - (1 sat debugiranja poslje :( ) 
- asseti se sada ispravno ucitavaju
- napravio test za mainBar. meni se ispravno prikazuje. Ali se mainBar sastoji od 5 elemenata 
  tako da se sada prikazuje samo podloga ali sve bi sada trebalo raditi.
